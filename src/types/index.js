export const MESSAGE_STATUS = {
  SENT: "sent",
  SEEN: "seen",
  DELETED: "deleted",
};
