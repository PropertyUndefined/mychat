import * as actiontypes from "../actions/actiontypes";
import { createReducer } from "@reduxjs/toolkit";

export const userSettingsReducer = createReducer(
  {
    name: "",
    phone: "",
    info: "",
    tag: "",
    imageName: "",
    photoLink: "",
    showStatusMessage: false,
    errorMessage: "",
    tagIsUnique: true,
    formValid: true,
    progress: 0,
    progressMessage: "",
  },
  {
    [actiontypes.CHANGE_NAME]: (state, action) => {
      state.name = action.payload;
    },
    [actiontypes.CHANGE_PHONE]: (state, action) => {
      state.phone = action.payload;
    },
    [actiontypes.UPLOAD_IMAGE]: (state, action) => {
      state.imageName = action.payload.imageName;
    },
    [actiontypes.CHANGE_TAG]: (state, action) => {
      state.tag = action.payload;
    },
    [actiontypes.CHANGE_INFO]: (state, action) => {
      state.info = action.payload;
    },
    [actiontypes.CHECK_TAG_UNIQUENESS_SUCCESS]: (state, action) => {
      state.tagIsUnique = action.payload;
      if (state.tagIsUnique) {
        state.formValid = true;
      } else {
        state.formValid = false;
      }
    },
    [actiontypes.FETCH_PROFILE_INFO_SUCCESS]: (state, action) => {
      state.name = action.payload.name;
      state.phone = action.payload.phone;
      state.info = action.payload.info;
      state.tag = action.payload.tag;
      state.photoLink = action.payload.photoLink;
      let nonEmpty = 0;
      for (let property in action.payload) {
        if (property === "email" || property === "friendsList") {
          continue;
        }
        action.payload[property] !== "" && nonEmpty++;
      }
      state.progress = (nonEmpty / 5) * 100;
      if (state.progress === 100) {
        state.progressMessage = "Good job filling out all the fields!";
      } else if (state.progress === 0) {
        state.progressMessage =
          "Start filling out those fields to increase your progress!";
      } else {
        state.progressMessage = `Good job! Now try to fill out the rest ${
          100 - state.progress
        }%`;
      }
    },
    [actiontypes.FETCH_PROFILE_INFO_ERROR]: (state, action) => {
      state.showStatusMessage = true;
      state.errorMessage = action.error.message;
    },
    [actiontypes.CLEANUP_PROFILE_INFO]: (state, action) => {
      state.name = "";
      state.phone = "";
      state.info = "";
      state.tag = "";
      state.imageName = "";
      state.photoLink = "";
      state.showStatusMessage = false;
      state.erorrMessage = "";
      state.tagIsUnique = true;
      state.formValid = true;
      state.progress = 0;
      state.progressMessage = "";
    },
  }
);
