import * as actiontypes from "../actions/actiontypes";
import { createReducer } from "@reduxjs/toolkit";
import { MESSAGE_STATUS } from "../types/index";

export const chatsReducer = createReducer(
  {
    chats: [],
    editMode: false,
    activeChatID: "",
    selectedMessageID: "",
    actionBoardShown: false,
    activeChatMessages: [],
    chatName: "",
    message: "",
  },
  {
    [actiontypes.FETCH_CHATS_SUCCESS]: (state, action) => {
      state.chats = action.payload;
    },
    [actiontypes.CHANGE_CHAT_NAME]: (state, action) => {
      state.chatName = action.payload.name;
      state.activeChatID = action.payload.chatID;
      state.activeChatMessages = state.chats.filter((chat) => {
        return chat.chatID === state.activeChatID;
      })[0].messages;
    },
    [actiontypes.CHANGE_MESSAGES_STATUS]: (state, { payload }) => {
      const currentUserId = payload.currentUserId;
      const currentChat = state.chats.find((chat) => {
        return chat.chatID === state.activeChatID;
      });
      currentChat.messages.forEach((message) => {
        if (
          currentUserId === message.sender &&
          message.status !== MESSAGE_STATUS.DELETED
        ) {
          message.status = MESSAGE_STATUS.SEEN;
        }
      });
      state.activeChatMessages.forEach((message) => {
        if (
          currentUserId === message.sender &&
          message.status !== MESSAGE_STATUS.DELETED
        ) {
          message.status = MESSAGE_STATUS.SEEN;
        }
      });
    },
    [actiontypes.CHANGE_MESSAGE]: (state, action) => {
      state.message = action.payload;
    },
    [actiontypes.SUBMIT_MESSAGE]: (state, action) => {
      let currentChat = state.chats.filter((chat) => {
        return chat.chatID === state.activeChatID;
      });
      currentChat[0].messages.push({
        sender: action.payload.sender,
        messageID: action.payload.id,
        text: action.payload.text,
        status: "pending",
      });
      state.activeChatMessages.push({
        sender: action.payload.sender,
        messageID: action.payload.id,
        text: action.payload.text,
        status: "pending",
      });
      state.message = "";
    },
    [actiontypes.SEND_MESSAGE_SUCCESS]: (state, action) => {
      let currentChat = state.chats.filter((chat) => {
        return chat.chatID === state.activeChatID;
      });
      let selectedMessage = currentChat[0].messages.filter((message) => {
        return message.messageID === action.payload.messageID;
      });
      selectedMessage[0].status = action.payload.status;
      let selectedCurrentMessage = state.activeChatMessages.filter(
        (message) => {
          return message.messageID === action.payload.messageID;
        }
      );
      selectedCurrentMessage[0].status = action.payload.status;
    },
    [actiontypes.SELECT_MESSAGE]: (state, action) => {
      state.selectedMessageID = action.payload;
      state.actionBoardShown = true;
    },
    [actiontypes.ACTION_BOARD_CANCEL]: (state, action) => {
      state.selectedMessageID = "";
      state.actionBoardShown = false;
      state.editMode = false;
      state.message = "";
    },
    [actiontypes.ACTION_BOARD_DELETE_SUCCESS]: (state, action) => {
      let currentChat = state.chats.filter((chat) => {
        return chat.chatID === state.activeChatID;
      });
      let selectedMessage = currentChat[0].messages.filter((message) => {
        return message.messageID === action.payload.messageID;
      });
      selectedMessage[0].status = action.payload.status;
      let selectedCurrentMessage = state.activeChatMessages.filter(
        (message) => {
          return message.messageID === action.payload.messageID;
        }
      );
      selectedCurrentMessage[0].status = action.payload.status;
      state.selectedMessageID = "";
      state.actionBoardShown = false;
      state.message = "";
      state.editMode = false;
    },
    [actiontypes.ACTION_BOARD_EDIT]: (state, action) => {
      state.editMode = true;
      let messageToEdit = state.activeChatMessages.filter((message) => {
        return message.messageID === state.selectedMessageID;
      });
      state.message = messageToEdit[0].text;
    },
    [actiontypes.SUBMIT_MESSAGE_EDIT]: (state, action) => {
      let messageID = state.selectedMessageID;
      let currentChat = state.chats.filter((chat) => {
        return chat.chatID === state.activeChatID;
      });
      let selectedMessage = currentChat[0].messages.filter((message) => {
        return message.messageID === messageID;
      });
      selectedMessage[0].edited = true;
      selectedMessage[0].text = action.payload.text;
      let selectedCurrentMessage = state.activeChatMessages.filter(
        (message) => {
          return message.messageID === messageID;
        }
      );
      selectedCurrentMessage[0].edited = true;
      selectedCurrentMessage[0].text = action.payload.text;
    },
    [actiontypes.EDIT_MESSAGE_SUCCESS]: (state, action) => {
      state.selectedMessageID = "";
      state.actionBoardShown = false;
      state.message = "";
      state.editMode = false;
    },
    [actiontypes.CLEANUP_CHATS]: (state, action) => {
      state.chats = [];
      state.editMode = false;
      state.chatName = "";
      state.message = "";
      state.activeChatID = "";
      state.actionBoardShown = false;
      state.selectedMessageID = "";
      state.activeChatMessages = [];
    },
  }
);
