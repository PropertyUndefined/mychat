import * as actiontypes from "../actions/actiontypes";
import { createReducer } from "@reduxjs/toolkit";

export const modalReducer = createReducer(
  { showModal: false},
  {
    [actiontypes.OPEN_MODAL]: (state, action) => {
      state.showModal = true;
    },
    [actiontypes.CLOSE_MODAL]: (state, action) => {
      state.showModal = false;
    },
  }
);
