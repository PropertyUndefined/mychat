import { firebaseService } from "./firebaseConfig";
import "firebase/firestore";

const db = firebaseService.firestore();

export default db;
