import { firebaseService } from "./firebaseConfig";
import "firebase/storage";

const storageRef = firebaseService.storage().ref();

export default storageRef;
