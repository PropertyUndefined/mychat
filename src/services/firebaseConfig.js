import * as firebase from "firebase/app";

const CONFIG = {
  apiKey: "AIzaSyC17Z3A36kIO-06NQiKuP1UGLxC80GOTQg",
  authDomain: "mychat-95663.firebaseapp.com",
  databaseURL: "https://mychat-95663.firebaseio.com",
  projectId: "mychat-95663",
  storageBucket: "mychat-95663.appspot.com",
  messagingSenderId: "36919202048",
  appId: "1:36919202048:web:78a50554cfa30b54fce3e5",
};

export const firebaseService = firebase.initializeApp(CONFIG);
