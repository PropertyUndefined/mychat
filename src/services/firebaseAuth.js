import { firebaseService } from "./firebaseConfig";
import "firebase/auth";

export const auth = firebaseService.auth();
