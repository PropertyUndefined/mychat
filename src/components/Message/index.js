import React from "react";
import { connect } from "react-redux";
import styles from "./chat-message.module.scss";
import { selectMessage } from "../../actions/actions";
import { MESSAGE_STATUS } from "../../types";

const Message = ({
  message,
  editMode,
  selectedMessageID,
  selectMessage,
  currentUserID,
}) => {
  const isSent = message.sender === currentUserID;

  const handleClick = () => {
    if (
      message.status === MESSAGE_STATUS.DELETED ||
      editMode === true ||
      !isSent
    ) {
      return;
    }
    selectMessage(message.messageID);
  };
  return (
    <div
      onClick={handleClick}
      className={`${styles.container} ${
        message.messageID === selectedMessageID ? styles.selected : ""
      } ${message.status === MESSAGE_STATUS.DELETED ? styles.deleted : ""}
      ${isSent ? styles.left : styles.right}`}
    >
      <p className={`${styles.msg} ${isSent ? styles.sent : styles.received}`}>
        {message.status === MESSAGE_STATUS.DELETED
          ? "This message has been deleted ❌"
          : message.text}
      </p>
      {message.status !== MESSAGE_STATUS.DELETED && (
        <h5 className={styles.status}>{`${isSent ? message.status : ""} ${
          message.edited ? "✏️ edited" : ""
        }`}</h5>
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    selectedMessageID: state.chats.selectedMessageID,
    editMode: state.chats.editMode,
    currentUserID: state.userStatus.user.uid,
  };
};

export default connect(mapStateToProps, { selectMessage })(Message);
