import React, { useEffect } from "react";
import { connect } from "react-redux";
import MessageList from "../MessageList/index";
import MessageActionBoard from "../MessageActionBoard/index";
import {
  cleanupChats,
  changeMessage,
  sendMessage,
  editMessage,
} from "../../actions/actions";
import styles from "./chat.module.scss";

const ChatWindow = ({
  chatName,
  editMode,
  message,
  messages,
  editMessage,
  cleanupChats,
  changeMessage,
  sendMessage,
}) => {
  useEffect(() => {
    return () => {
      cleanupChats();
    };
  }, [cleanupChats]);

  const handleChange = (event) => {
    changeMessage(event.target.value);
  };

  const handleClick = () => {
    if (editMode) {
      editMessage();
    } else {
      sendMessage(message);
    }
  };

  return (
    <div className={styles.window}>
      <div className={styles.header}>
        <div className={styles.name}>{chatName}</div>
      </div>
      <MessageList messages={messages} />
      <div className={styles.inputBox}>
        <input
          onChange={handleChange}
          className={styles.msgInput}
          placeholder="Type your message here"
          type="text"
          value={message}
        />
        <button className={styles.btn} onClick={handleClick}>
          {editMode ? "EDIT" : "SEND"}
        </button>
      </div>
      <MessageActionBoard />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    chatName: state.chats.chatName,
    message: state.chats.message,
    editMode: state.chats.editMode,
  };
};

export default connect(mapStateToProps, {
  cleanupChats,
  changeMessage,
  sendMessage,
  editMessage,
})(ChatWindow);
