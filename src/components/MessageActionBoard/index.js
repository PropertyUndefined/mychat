import React from "react";
import { connect } from "react-redux";
import styles from "./action-board.module.scss";
import {
  actionBoardCancel,
  actionBoardDelete,
  actionBoardEdit,
} from "../../actions/actions";

const MessageActionBoard = ({
  isShown,
  actionBoardDelete,
  actionBoardCancel,
  actionBoardEdit,
}) => {
  const handleCancelClick = () => {
    actionBoardCancel();
  };

  const handleDeleteClick = () => {
    actionBoardDelete();
  };

  const handleEditClick = () => {
    actionBoardEdit();
  };

  return (
    <div className={`${styles.container} ${isShown ? styles.slide : ""}`}>
      <span>What do you want to do?</span>
      <button className={styles.btn} onClick={handleEditClick}>
        Edit
      </button>
      <button className={styles.btn} onClick={handleDeleteClick}>
        Delete
      </button>
      <button className={styles.btn} onClick={handleCancelClick}>
        Cancel
      </button>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    isShown: state.chats.actionBoardShown,
  };
};

export default connect(mapStateToProps, {
  actionBoardCancel,
  actionBoardDelete,
  actionBoardEdit,
})(MessageActionBoard);
