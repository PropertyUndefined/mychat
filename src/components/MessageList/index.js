import React, { useRef, useEffect } from "react";
import Message from "../Message/index";
import styles from "./message-list.module.scss";

const MessageList = ({ messages }) => {
  const lastMessage = useRef(null);

  useEffect(() => {
    lastMessage.current.scrollIntoView({ behavior: "smooth" });
  }, [messages]);

  return (
    <div className={styles.container}>
      <div className={styles.inner}>
        {messages.map((message) => {
          return <Message key={message.messageID} message={message} />;
        })}
        <div ref={lastMessage}></div>
      </div>
    </div>
  );
};

export default MessageList;
