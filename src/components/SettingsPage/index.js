import React, { useEffect, useState } from "react";
import styles from "./settings.module.scss";
import InfoMessage from "../InfoMessage/index";
import ProfileProgress from "../ProfileProgress/index";
import { ReactComponent as LoadPhotoSVG } from "../../SVG/LoadPhoto.svg";
import { connect } from "react-redux";
import {
  changeName,
  changePhone,
  changeTag,
  changeInfo,
  uploadImage,
  fetchProfileInfo,
  cleanupProfileInfo,
  postSettings,
  postSettingsWithPhoto,
  checkTagUniqueness,
} from "../../actions/actions";
//todo: a bug: default pic for profiles looks weird

const SettingsPage = ({
  tagIsUnique,
  formValid,
  name,
  userPhone,
  changeName,
  changeTag,
  changeInfo,
  uploadImage,
  changePhone,
  postSettings,
  postSettingsWithPhoto,
  fetchProfileInfo,
  cleanupProfileInfo,
  userEmail,
  showStatusMessage,
  checkTagUniqueness,
  errorMessage,
  userTag,
  photoLink,
  userInfo,
  userImageName,
}) => {
  const [myFile, setMyFile] = useState(null);

  useEffect(() => {
    fetchProfileInfo({ email: userEmail });
    return () => {
      cleanupProfileInfo();
    };
  }, [fetchProfileInfo, userEmail, cleanupProfileInfo]);

  useEffect(() => {
    if (myFile !== null) {
      uploadImage({ imageName: myFile.name });
    }
  }, [myFile, uploadImage]);

  const handleNameChange = (event) => {
    changeName(event.target.value);
  };

  const handlePhoneChange = (event) => {
    changePhone(event.target.value);
  };

  const handleTagChange = (event) => {
    changeTag(event.target.value);
  };

  const handleInfoChange = (event) => {
    changeInfo(event.target.value);
  };

  const handleImageChange = (event) => {
    let currentFile = event.target.files[0];
    setMyFile(currentFile);
  };

  const handleTagUnfocus = (event) => {
    checkTagUniqueness(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (myFile === null) {
      postSettings({
        email: userEmail,
        name: name,
        phone: userPhone,
        tag: userTag,
        info: userInfo,
      });
    } else {
      postSettingsWithPhoto({
        email: userEmail,
        name: name,
        phone: userPhone,
        tag: userTag,
        info: userInfo,
        photo: myFile,
      });
    }
  };

  return (
    <div className={`${styles.container} l-content`}>
      <div className={styles.title}>Profile information</div>
      <ProfileProgress />
      {showStatusMessage && (
        <InfoMessage message={errorMessage} isError={true} />
      )}
      <form className={styles.form}>
        <label className={styles.fileLabel}>
          Change photo:
          <input
            onChange={handleImageChange}
            type="file"
            accept=".png, .jpg, .jpeg"
          />
          <span className={`${styles.input} ${styles.photoInput}`}>
            {photoLink !== undefined ? (
              <img src={photoLink} alt="profile" />
            ) : (
              <LoadPhotoSVG />
            )}
          </span>
          <div className={styles.imageFilename}>{userImageName}</div>
        </label>
        <label htmlFor="name">Name:</label>
        <input
          className={styles.input}
          placeholder="name"
          onChange={handleNameChange}
          type="text"
          value={name}
          id="name"
        />
        <label htmlFor="phone">Phone number:</label>
        <input
          className={styles.input}
          placeholder="phone number"
          onChange={handlePhoneChange}
          type="tel"
          value={userPhone}
          id="phone"
        />
        <label htmlFor="tag">User tag:</label>
        <input
          onBlur={handleTagUnfocus}
          className={styles.input}
          placeholder="user tag"
          onChange={handleTagChange}
          type="text"
          value={userTag}
          id="tag"
        />
        {!tagIsUnique && (
          <InfoMessage
            message="Sorry, this tag has laready been taken"
            isError={true}
          />
        )}
        <label htmlFor="info">Info about you:</label>
        <textarea
          className={`${styles.input} ${styles.info}`}
          placeholder="info"
          onChange={handleInfoChange}
          type="text"
          value={userInfo}
          id="info"
        />
        <button
          className={`${styles.btn} ${formValid ? "" : styles.disabled}`}
          onClick={handleSubmit}
          type="submit"
        >
          Apply settings
        </button>
      </form>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    tagIsUnique: state.userSettings.tagIsUnique,
    name: state.userSettings.name,
    userEmail: state.userStatus.user.email,
    userPhone: state.userSettings.phone,
    userTag: state.userSettings.tag,
    userInfo: state.userSettings.info,
    userImageName: state.userSettings.imageName,
    photoLink: state.userSettings.photoLink,
    showStatusMessage: state.userSettings.showStatusMessage,
    errorMessage: state.userSettings.errorMessage,
    formValid: state.userSettings.formValid,
  };
};

export default connect(mapStateToProps, {
  changeName,
  changePhone,
  changeTag,
  changeInfo,
  uploadImage,
  postSettings,
  postSettingsWithPhoto,
  fetchProfileInfo,
  cleanupProfileInfo,
  checkTagUniqueness,
})(SettingsPage);
