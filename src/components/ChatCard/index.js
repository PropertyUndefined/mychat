import React from "react";
import { useRouteMatch } from "react-router-dom";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { changeChatName, readReceivedMessages } from "../../actions/actions";
import styles from "./chat-card.module.scss";

const ChatCard = ({
  imgsrc,
  name,
  chatID,
  activeChatID,
  changeChatName,
  readReceivedMessages,
}) => {
  const { url } = useRouteMatch();
  const handleClick = () => {
    changeChatName({ name: name, chatID: chatID });
    readReceivedMessages();
  };

  return (
    <Link
      onClick={handleClick}
      to={`${url}/${chatID}`}
      className={`${styles.container} ${
        chatID === activeChatID ? styles.active : ""
      }`}
    >
      <div className={styles.image}>
        <img src={imgsrc} alt="profile" />
      </div>
      <div className={styles.name}>{name}</div>
    </Link>
  );
};

const mapStateToProps = (state) => {
  return {
    activeChatID: state.chats.activeChatID,
  };
};

export default connect(mapStateToProps, {
  changeChatName,
  readReceivedMessages,
})(ChatCard);
