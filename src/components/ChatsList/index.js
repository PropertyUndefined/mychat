import React from "react";
import InfoMessage from "../InfoMessage/index";
import ChatCard from "../ChatCard/index";

const ChatsList = ({ chats }) => {
  if (chats.length === 0) {
    return (
      <div>
        <InfoMessage
          isError={false}
          message="You do not have any chats listed yet."
        />
      </div>
    );
  }
  return (
    <div>
      {chats.map((chat) => {
        return (
          <ChatCard
            key={chat.chatID}
            chatID={chat.chatID}
            name={chat.name}
            imgsrc={chat.photoLink}
          />
        );
      })}
    </div>
  );
};

export default ChatsList;
