import React, { useEffect } from "react";
import { useRouteMatch, Switch } from "react-router-dom";
import PrivateRoute from "../PrivateRoute";
import { connect } from "react-redux";
import ChatWindow from "../ChatWindow/index";
import ChatsList from "../ChatsList/index";
import { fetchChats } from "../../actions/actions";
import styles from "./chats-page.module.scss";

const ChatsPage = ({ fetchChats, chats, messages }) => {
  const { path } = useRouteMatch();

  useEffect(() => {
    fetchChats();
  }, [fetchChats]);

  return (
    <div className={`l-content ${styles.container}`}>
      <ChatsList chats={chats} />
      <Switch>
        <PrivateRoute path={`${path}/:id`}>
          <ChatWindow messages={messages} />
        </PrivateRoute>
      </Switch>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    chats: state.chats.chats,
    messages: state.chats.activeChatMessages,
  };
};

export default connect(mapStateToProps, { fetchChats })(ChatsPage);
