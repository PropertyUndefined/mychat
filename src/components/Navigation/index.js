import React from "react";
import { connect } from "react-redux";
import { showLogin, showSignup, signoutUser } from "../../actions/actions";
import { Link } from "react-router-dom";
import styles from "./navigation.module.scss";
import { openModal } from "../../actions/actions";

const Navigation = ({
  openModal,
  showLogin,
  showSignup,
  signoutUser,
  isAuthenticated,
}) => {
  const handleSignupClick = () => {
    openModal();
    showSignup();
  };

  const handleLoginClick = () => {
    openModal();
    showLogin();
  };

  const handleSignoutClick = (e) => {
    signoutUser();
  };

  return (
    <div className={styles.container}>
      <div className={`${styles.item} ${styles["item--home"]}`}>
        <Link to="/">Home</Link>
      </div>
      <div className={styles.inner}>
        {!isAuthenticated && (
          <div className={styles.item} onClick={handleSignupClick}>
            <span>Sign up</span>
          </div>
        )}
        {!isAuthenticated && (
          <div onClick={handleLoginClick} className={styles.item}>
            <span>Log in</span>
          </div>
        )}
        {isAuthenticated && (
          <div className={styles.item}>
            <Link to="options">Options</Link>
          </div>
        )}
        {isAuthenticated && (
          <div className={styles.item}>
            <Link to="chats">Chats</Link>
          </div>
        )}
        {isAuthenticated && (
          <div onClick={handleSignoutClick} className={styles.item}>
            <span>Logout</span>
          </div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.userStatus.isAuthenticated,
  };
};

export default connect(mapStateToProps, {
  openModal,
  showSignup,
  showLogin,
  signoutUser,
})(Navigation);
