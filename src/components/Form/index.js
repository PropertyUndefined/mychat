import React, { useEffect } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { connect } from "react-redux";
import styles from "./form.module.scss";
import {
  changeEmail,
  changePassword,
  loginUser,
  resetForm,
  loginUserWithModal,
  signupUser,
} from "../../actions/actions";

const Form = ({
  type,
  inModal,
  resetForm,
  changeEmail,
  changePassword,
  loginUser,
  loginUserWithModal,
  signupUser,
  email,
  password,
  hasError,
  errorMessage,
}) => {
  useEffect(() => {
    return () => {
      resetForm();
    };
  }, [resetForm]);

  const history = useHistory();
  const location = useLocation();
  const { from } = location.state || { from: { pathname: "/" } };

  const handleModalClick = (event) => {
    event.stopPropagation();
  };

  const handleEmailChange = (event) => {
    changeEmail(event.target.value);
  };

  const handlePasswordChange = (event) => {
    changePassword(event.target.value);
  };

  const handleLoginSubmit = (event) => {
    event.preventDefault();
    if (inModal) {
      loginUserWithModal({ email: email, password: password });
    } else {
      loginUser({ email: email, password: password }).then((
      ) => {
        history.replace(from);
      });
    }
  };

  const handleSignupUser = (event) => {
    event.preventDefault();
    signupUser({ email: email, password: password });
  };

  return (
    <div onClick={handleModalClick} className={styles.container}>
      <div className={styles.title}>
        {type === "login" ? "Please, log in" : "Please, sign up"}
      </div>
      {hasError && (
        <div className={styles.title}>Sorry, it seems, an error's occured</div>
      )}
      <div className={styles.form}>
        <input
          onChange={handleEmailChange}
          placeholder="email"
          value={email}
          type="email"
          className={styles.input}
        />
        <input
          onChange={handlePasswordChange}
          placeholder="password"
          value={password}
          type="password"
          className={styles.input}
        />
        <button
          onClick={type === "login" ? handleLoginSubmit : handleSignupUser}
          type="submit"
          className={styles.btn}
        >
          {type === "login" ? "Log in" : "Sign up"}
        </button>
        <div className={styles.title}>{errorMessage}</div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    type: state.form.type,
    email: state.form.email,
    password: state.form.password,
    hasError: state.form.hasError,
    errorMessage: state.form.errorMessage,
  };
};

export default connect(mapStateToProps, {
  changeEmail,
  changePassword,
  loginUser,
  loginUserWithModal,
  signupUser,
  resetForm,
})(Form);
