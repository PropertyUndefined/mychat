import React, { useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import {
  changeUserStatusLogin,
  changeUserStatusSignout,
} from "./actions/actions";
import { setAuthStateListener } from "./controllers/firebaseController";
import PrivateRoute from "./components/PrivateRoute";
import Form from "./components/Form";
import { connect } from "react-redux";
import SettingsPage from "./components/SettingsPage/index";
import HomePage from "./components/HomePage/index";
import FriendsPage from "./components/FriendsPage/index";
import Navigation from "./components/Navigation/index";
import Modal from "./components/Modal/index";
import ChatsPage from "./components/ChatsPage/index";
import "./App.css";

function App({
  changeUserStatusLogin,
  changeUserStatusSignout,
  isAuthenticated,
}) {
  useEffect(() => {
    const authListener = setAuthStateListener((user) => {
      if (user) {
        changeUserStatusLogin(user);
      } else {
        changeUserStatusSignout(user);
      }
    });

    return authListener;
  }, [changeUserStatusLogin, changeUserStatusSignout]);

  return (
    <Router>
      <Modal />
      <Navigation />
      <Switch>
        <Route path="/" exact>
          {isAuthenticated ? <FriendsPage /> : <HomePage />}
        </Route>
        <Route path="/login">
          <Form inModal={false} />
        </Route>
        <PrivateRoute path="/options">
          <SettingsPage />
        </PrivateRoute>
        <PrivateRoute path="/chats">
          <ChatsPage />
        </PrivateRoute>
      </Switch>
    </Router>
  );
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.userStatus.isAuthenticated,
  };
};

export default connect(mapStateToProps, {
  changeUserStatusLogin,
  changeUserStatusSignout,
})(App);
